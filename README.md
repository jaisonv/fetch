# Running the app

Simply open the file project ```Fetch.xcodeproj``` and run the app.

>The project used Swift Lint to help with code smell. It's not mandatory to run the app, but if you want to check it and don't have it installed, follow [these instructions](https://github.com/realm/SwiftLint#installation) for installation. 

# Requirements
Xcode 12+
iOS 13+

The project was created on Xcode 12.1 and I tried running on iOS version as low as 13.0. Due to the new Xcode adding SceneDelegate file it's not possible to run on iOS versions lower that 13.

# Assignment

Display this list of items to the user based on the following requirements:

- Display all the items grouped by "listId"
>I created a UITableViewController with sections representing each different ```listId```.

- Sort the results first by "listId" then by "name" when displaying.
>Items were sorted by name at the time we load the array from backend.

- Filter out any items where "name" is blank or null.
>Items with name ```nil``` or empty were removed at the time we load the array from backend (before sorting).

# Description

![app-gif](app.gif)

My focus was to run for iPhone device.

Code is structured using MVVM. Trying to be the more testable as possible with dependency injection.

I display emojis to represent the ```listId``` number and make the UI less boring (I'm not a UI expert though 😅).

I wrote some unit tests trying to cover as more as I could and got to 84% coverage.

For building the UIs I use **Auto Layout anchors** which is native and not so hard to use. I'm more familiar to using **Snapkit** daily at work but for side projects I like using this option to avoid adding third party dependencies.

If you have any question feel free to ask me.

