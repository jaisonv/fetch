//
//  APIClientTests.swift
//  FetchTests
//
//  Created by Jaison Vieira on 10/28/20.
//

import XCTest
@testable import Fetch

class APIClientTests: XCTestCase {

    func testRequestObjectIsSuccessful() {
        let data = makeDictionaryData()

        let session = MockSession(response: (data, URLResponse: nil, error: nil))
        let sut = APIClient(session: session)

        let expectation = self.expectation(description: "Send mock request")

        let completion: (Result<[String: String], Error>) -> Void = { result in
            switch result {
            case .failure(let error):
                XCTAssertNil(error)
            case .success(let response):
                XCTAssertEqual(["lorem": "ipsum"], response)
            }

            expectation.fulfill()
        }

        sut.request(MockEndpoint(), completion: completion)
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testRequestObjectListIsSuccessful() {
        let data = makeArrayData()

        let session = MockSession(response: (data, URLResponse: nil, error: nil))
        let sut = APIClient(session: session)

        let expectation = self.expectation(description: "Send mock request")

        let completion: (Result<[[String: String]], Error>) -> Void = { result in
            switch result {
            case .failure(let error):
                XCTAssertNil(error)
            case .success(let response):
                XCTAssertNotNil(response)
            }

            expectation.fulfill()
        }

        sut.request(MockEndpoint(), completion: completion)
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testRequestObjectIsNotSuccessful() {
        let session = MockSession(response: (nil, URLResponse: nil, error: APIError.unknown))
        let sut = APIClient(session: session)

        let expectation = self.expectation(description: "Send mock request")

        let completion: (Result<[String: String], Error>) -> Void = { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error as? APIError, APIError.unknown)
            case .success(let response):
                XCTAssertNil(response)
            }

            expectation.fulfill()
        }

        sut.request(MockEndpoint(), completion: completion)
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testRequestObjectHasIncorrectData() {
        let data = "".data(using: .utf8)

        let session = MockSession(response: (data, URLResponse: nil, error: nil))
        let sut = APIClient(session: session)

        let expectation = self.expectation(description: "Send mock request")

        let completion: (Result<[String: String], Error>) -> Void = { result in
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success(let response):
                XCTAssertNil(response)
            }

            expectation.fulfill()
        }

        sut.request(MockEndpoint(), completion: completion)
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testRequestObjectUnknownError() {

        let session = MockSession(response: (nil, URLResponse: nil, error: nil))
        let sut = APIClient(session: session)

        let expectation = self.expectation(description: "Send mock request")

        let completion: (Result<[String: String], Error>) -> Void = { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error as? APIError, APIError.unknown)
            case .success(let response):
                XCTAssertNil(response)
            }

            expectation.fulfill()
        }

        sut.request(MockEndpoint(), completion: completion)
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    private func makeDictionaryData() -> Data? {
        let json = """
        {"lorem": "ipsum"}
        """

        return json.data(using: .utf8)
    }

    private func makeArrayData() -> Data? {
        let json = """
        [{"lorem": "ipsum"},
        {"lorem": "ipsum"}]
        """

        return json.data(using: .utf8)
    }
}
