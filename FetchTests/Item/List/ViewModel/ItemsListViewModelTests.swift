//
//  ItemsListViewModelTests.swift
//  FetchTests
//
//  Created by Jaison Vieira on 10/28/20.
//

import XCTest
@testable import Fetch

class ItemsListViewModelTests: XCTestCase {

    func testFetchItemsSuccess() {
        let sut = makeSUT()
        sut.fetchItems()

        switch sut.fetchResultState {
        case .notLoaded:
            XCTAssert(false, "Request not started.")
        case .loaded(let items):
            XCTAssertGreaterThan(items.count, 0)
        case .error(let message):
            XCTAssert(false, message)
        }
    }

    func testFetchItemsFail() {
        let sut = makeFailingSUT()
        sut.fetchItems()

        switch sut.fetchResultState {
        case .notLoaded:
            XCTAssert(false, "Request not started.")
        case .loaded:
            XCTAssert(false, "This request should fail.")
        case .error(let message):
            XCTAssertNotNil(message)
        }
    }

    private func makeSUT() -> ItemsListViewModel {
        return ItemsListViewModel(apiClient: MockSession.makeValidAPIClient())
    }

    private func makeFailingSUT() -> ItemsListViewModel {
        return ItemsListViewModel(apiClient: MockSession.makeInvalidAPIClient())
    }
}
