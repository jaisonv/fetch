//
//  ItemsListTableViewControllerTests.swift
//  FetchTests
//
//  Created by Jaison Vieira on 10/28/20.
//

import XCTest
@testable import Fetch

class ItemsListTableViewControllerTests: XCTestCase {

    func testTableViewLoadsSuccess() {
        let sut = makeSUT()
        XCTAssertNotNil(sut.tableView)
    }

    func testTableViewNumberOfSectionsSuccess() {
        let sut = makeSUT()
        sut.view.layoutIfNeeded()

        XCTAssertEqual(sut.tableView.numberOfSections, makeNumberOfSections())
    }

    func testTableViewNumberOfRowsSuccess() {
        let sut = makeSUT()
        sut.view.layoutIfNeeded()

        let section = 0
        let items = makeItemsForSection(section + 1)

        XCTAssertEqual(sut.tableView.numberOfRows(inSection: section), items.count)
    }

    func testTableViewCellForItemSuccess() {
        let sut = makeSUT()
        sut.view.layoutIfNeeded()

        let cell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(item: 0, section: 0)) as? ItemTableViewCell

        XCTAssertNotNil(cell)
    }

    func testTableViewNumberOfRowsFail() {
        let sut = makeFailingSUT()
        sut.view.layoutIfNeeded()

        XCTAssertEqual(sut.tableView(sut.tableView, numberOfRowsInSection: 0), 0)
    }

    func testTableViewCellForItemFail() {
        let sut = makeFailingSUT()
        sut.view.layoutIfNeeded()

        let cell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(item: 0, section: 0)) as? ItemTableViewCell
        XCTAssertNil(cell)
    }

    private func makeSUT() -> ItemsListTableViewController {
        let viewModel = ItemsListViewModel(apiClient: MockSession.makeValidAPIClient())

        return ItemsListTableViewController(viewModel: viewModel)
    }

    private func makeFailingSUT() -> ItemsListTableViewController {
        let viewModel = ItemsListViewModel(apiClient: MockSession.makeInvalidAPIClient())

        return ItemsListTableViewController(viewModel: viewModel)
    }

    private func makeItemsArray() -> [Item] {
        let bundle = Bundle(for: MockSession.self)

        guard let url = bundle.url(forResource: "items", withExtension: "json"),
              let data = try? Data(contentsOf: url),
              var items = try? JSONDecoder().decode([Item].self, from: data) else {
            fatalError("Failed to load items.json.")
        }

        items.removeAll { item -> Bool in
            guard let name = item.name else { return true }
            return name.isEmpty
        }

        return items
    }

    private func makeNumberOfSections() -> Int {
        let items = makeItemsArray()
        return Set(items.map { $0.listId }).count
    }

    private func makeItemsForSection(_ section: Int) -> [Item] {
        let items = makeItemsArray()
        return items.filter { $0.listId == section }
    }
}
