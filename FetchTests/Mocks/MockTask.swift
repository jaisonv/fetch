//
//  MockTask.swift
//  FetchTests
//
//  Created by Jaison Vieira on 10/28/20.
//

import Foundation
@testable import Fetch

internal class MockTask: URLSessionDataTask {

    typealias Response = (data: Data?, URLResponse: URLResponse?, error: Error?)
    typealias Completion = ((Data?, URLResponse?, Error?) -> Void)

    var mockResponse: Response
    let completionHandler: Completion

    init(response: Response, completionHandler: @escaping Completion) {
        self.mockResponse = response
        self.completionHandler = completionHandler
    }

    override func resume() {
        completionHandler(mockResponse.data, mockResponse.URLResponse, mockResponse.error)
    }
}
