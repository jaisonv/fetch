//
//  MockEndpoint.swift
//  FetchTests
//
//  Created by Jaison Vieira on 10/28/20.
//

import Foundation
@testable import Fetch

internal class MockEndpoint: Endpoint {
    var path: String = ""
    var httpMethod: HTTPMethod = .get
}
