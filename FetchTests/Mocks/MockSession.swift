//
//  MockSession.swift
//  FetchTests
//
//  Created by Jaison Vieira on 10/28/20.
//

import Foundation
@testable import Fetch

internal class MockSession: URLSession {

    typealias Response = (data: Data?, URLResponse: URLResponse?, error: Error?)
    private let response: Response

    init(response: Response) {
        self.response = response
    }

    override func dataTask(with request: URLRequest, completionHandler:
                            @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return MockTask(response: response, completionHandler: completionHandler)
    }

    static func makeValidAPIClient() -> APIClient {
        let bundle = Bundle(for: MockSession.self)

        guard let url = bundle.url(forResource: "items", withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load items.json.")
        }

        return APIClient(session: MockSession(response: (data, URLResponse: nil, error: nil)))
    }

    static func makeInvalidAPIClient() -> APIClient {
        return APIClient(session: MockSession(response: (Data(), URLResponse: nil, error: nil)))
    }
}
