//
//  ItemEndpoint.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/28/20.
//

import Foundation

struct ItemEndpoint: Endpoint {
    var path = "/hiring.json"
    var httpMethod = HTTPMethod.get
}
