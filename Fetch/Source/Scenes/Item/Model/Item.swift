//
//  Item.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/28/20.
//

import Foundation

struct Item: Decodable {
    // swiftlint:disable identifier_name
    let id: Int
    let listId: Int
    let name: String?
}
