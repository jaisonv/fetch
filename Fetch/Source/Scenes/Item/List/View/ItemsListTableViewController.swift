//
//  ItemsListTableViewController.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/28/20.
//

import UIKit

final class ItemsListTableViewController: UITableViewController {

    private let viewModel: ItemsListViewModel
    // store filtered items to avoid filtering for every interaction
    private var storedItems = [Int: [Item]]()

    init(viewModel: ItemsListViewModel) {
        self.viewModel = viewModel
        super.init(style: .grouped)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Items"

        tableView.register(ItemTableViewCell.self, forCellReuseIdentifier: "cell")

        tableView.allowsSelection = false

        viewModel.delegate = self
        viewModel.fetchItems()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        switch viewModel.fetchResultState {
        case .loaded(let items):
            // get unique numbers for listId and use as section section key.
            // this can also be used to count how many sections we have.
            items.map { $0.listId }
                .forEach {
                    // make sure we don't store when it's already stored
                    if !storedItems.keys.contains($0) {
                        storedItems[$0] = []
                    }
                }
            return storedItems.keys.count
        case .error, .notLoaded:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch viewModel.fetchResultState {
        case .loaded(let items):

            // listId starts from 1. Adding 1 to section will give us the correct key for that group
            let sectionKey = section + 1

            // prevent from filtering in every interaction
            if storedItems[sectionKey]?.isEmpty != nil {
                storedItems[sectionKey] = items.filter { $0.listId == sectionKey }
            }

            return storedItems[sectionKey]?.count ?? 0
        case .error, .notLoaded:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.fetchResultState {
        case .loaded:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell",
                                                           for: indexPath) as? ItemTableViewCell else {
                fatalError("ItemTableViewCell could not be dequeued.")
            }
            let sectionItems = storedItems[indexPath.section + 1] ?? []
            cell.configure(viewModel: ItemViewModel(item: sectionItems[indexPath.row]))
            return cell

        case .error, .notLoaded:
            return UITableViewCell()
        }
    }
}

extension ItemsListTableViewController: ItemsListViewModelDelegate {
    func didFinishFetchRequest() {
        tableView.reloadData()

        switch viewModel.fetchResultState {
        case .error(let message):
            let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        default:
            return
        }
    }
}
