//
//  ItemTableViewCell.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/28/20.
//

import UIKit

final class ItemTableViewCell: UITableViewCell {

    private let iconLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    private let listIdLabel: UILabel = {
        let label = UILabel()
        label.textColor = .blue
        return label
    }()

    private let nameLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        iconLabel.text = nil
        nameLabel.text = nil
        listIdLabel.text = nil
    }

    private func setupView() {

        iconLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        listIdLabel.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(iconLabel)
        contentView.addSubview(nameLabel)
        contentView.addSubview(listIdLabel)

        NSLayoutConstraint.activate([
            iconLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15)
        ])

        NSLayoutConstraint.activate([
            nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: iconLabel.trailingAnchor, constant: 15)
        ])

        NSLayoutConstraint.activate([
            listIdLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            listIdLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15)
        ])
    }

    func configure(viewModel: ItemViewModel) {
        iconLabel.text = viewModel.emojiIcon
        nameLabel.text = viewModel.name
        listIdLabel.text = viewModel.listId
    }
}
