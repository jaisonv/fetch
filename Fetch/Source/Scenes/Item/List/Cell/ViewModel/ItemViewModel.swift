//
//  ItemViewModel.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/28/20.
//

import Foundation

final class ItemViewModel {

    private let item: Item

    var listId: String {
        return String(item.listId)
    }

    var name: String {
        return item.name ?? ""
    }

    var emojiIcon: String {
        guard let baseEmojiUnicodeScalar = "😀".unicodeScalars.last else { return "" }
        let intEmojiListId = Int(baseEmojiUnicodeScalar.value + UInt32(item.listId - 1))
        guard let unicodeScalarEmojiListId = Unicode.Scalar(intEmojiListId) else { return "" }
        let charEmojiListId = Character(unicodeScalarEmojiListId)
        return String(charEmojiListId)
    }

    init(item: Item) {
        self.item = item
    }
}
