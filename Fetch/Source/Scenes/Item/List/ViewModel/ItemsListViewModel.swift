//
//  ItemsListViewModel.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/28/20.
//

import Foundation

protocol ItemsListViewModelDelegate: AnyObject {
    func didFinishFetchRequest()
}

enum ItemResultState {
    case notLoaded
    case loaded(items: [Item])
    case error(message: String)
}

final class ItemsListViewModel {

    private let apiClient: APIClient
    weak var delegate: ItemsListViewModelDelegate?
    private(set) var fetchResultState = ItemResultState.notLoaded

    init(apiClient: APIClient = APIClient()) {
        self.apiClient = apiClient
    }

    func fetchItems() {
        let completion: (Result<[Item], Error>) -> Void = { [weak self] result in
            switch result {
            case .failure(let error):
                self?.fetchResultState = .error(message: error.localizedDescription)
            case .success(var response):
                // remove item which name is nil or empty
                response.removeAll { item -> Bool in
                    guard let name = item.name else { return true }
                    return name.isEmpty
                }

                // sort by id (since name is always followed by id we can assume it is sorted by name)
                response.sort { $0.id < $1.id }

                self?.fetchResultState = .loaded(items: response)
            }
            DispatchQueue.main.async {
                self?.delegate?.didFinishFetchRequest()
            }
        }
        apiClient.request(ItemEndpoint(), completion: completion)
    }
}
