//
//  HTTPMethod.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/27/20.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
}
