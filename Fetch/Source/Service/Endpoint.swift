//
//  Endpoint.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/27/20.
//

import Foundation

protocol Endpoint {
    var path: String { get }
    var httpMethod: HTTPMethod { get }
}

extension Endpoint {
    var baseURL: String {
        return "https://fetch-hiring.s3.amazonaws.com"
    }
}
