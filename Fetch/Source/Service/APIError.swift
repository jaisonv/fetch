//
//  APIError.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/27/20.
//

import Foundation

enum APIError: Error, Equatable {
    case missingData
    case invalidUrl(_ url: String)
    case unknown
}

extension APIError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .missingData:
            return NSLocalizedString("Data is missing from the server.", comment: "Missing data")
        case .invalidUrl(let url):
            return NSLocalizedString("The URL used on this request is invalid: \(url)", comment: "Invalid URL")
        case .unknown:
            return NSLocalizedString("Unexpected error.", comment: "Unknown error")
        }
    }
}
