//
//  APIClient.swift
//  Fetch
//
//  Created by Jaison Vieira on 10/27/20.
//

import Foundation

struct APIClient {

    private let session: URLSession

    init(session: URLSession = URLSession.shared) {
        self.session = session
    }

    func request<T: Decodable>(_ endpoint: Endpoint, completion: @escaping (Result<T, Error>) -> Void) {

        let request = buildRequest(from: endpoint)
        session.dataTask(with: request) { (data, _, error) in
            if let error = error {
                completion(.failure(error))
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let model = try decoder.decode(T.self, from: data)
                    completion(.success(model))
                } catch {
                    completion(.failure(APIError.missingData))
                }
            } else {
                completion(.failure(APIError.unknown))
            }
        }.resume()
    }

    private func buildRequest(from endpoint: Endpoint) -> URLRequest {
        guard let url = URL(string: endpoint.baseURL)?.appendingPathComponent(endpoint.path) else {
            fatalError("Error building url: Set the correct URL Endpoint file.")
        }
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.httpMethod.rawValue

        return request
    }
}
